#!/usr/bin/env sh

# Required variables:
# SCW_ACCESS_KEY
# SCW_SECRET_KEY
# SCW_DEFAULT_ORGANIZATION_ID
# SCW_DEFAULT_PROJECT_ID
# SCW_DEFAULT_ZONE
# VOLUME_HANDLE
# SNAPSHOT_NAME

set -e

SCW_VOLUME=$(echo $VOLUME_HANDLE | sed 's/.*\///')
echo "Scaleway volume ID: $SCW_VOLUME"
scw instance snapshot create volume-id="$SCW_VOLUME" name="$SNAPSHOT_NAME-$(date +"%Y%m%d-%H%M%S")"
