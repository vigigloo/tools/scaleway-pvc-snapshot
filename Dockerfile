FROM curlimages/curl AS download

ARG SCW_VERSION

WORKDIR /home/curl_user

RUN curl -L -o scw "https://github.com/scaleway/scaleway-cli/releases/download/v${SCW_VERSION}/scaleway-cli_${SCW_VERSION}_linux_amd64"
RUN chmod +x scw

FROM alpine:3.15

COPY --from=download /home/curl_user/scw /usr/local/bin/scw

COPY snapshots.sh /snapshots.sh

RUN scw version

ENTRYPOINT ["/snapshots.sh"]
